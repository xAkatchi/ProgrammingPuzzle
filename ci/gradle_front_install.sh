#!/bin/bash

# Only install the dependencies for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install libxss1 for Chromium
apt update -yqq
apt install libxss1 -yqq

# Install libgconf-2-4 for Chromium
apt install libgconf-2-4 -yqq