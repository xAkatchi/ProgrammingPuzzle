/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package io.codekrijger.programming_puzzle.service.mapper;
