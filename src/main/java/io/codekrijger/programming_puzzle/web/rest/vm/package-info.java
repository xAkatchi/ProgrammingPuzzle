/**
 * View Models used by Spring MVC REST controllers.
 */
package io.codekrijger.programming_puzzle.web.rest.vm;
